package bnc.P5Sensors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import netP5.NetAddress;
import oscP5.OscP5;
import processing.core.PApplet;
import processing.core.PFont;
import processing.touchProcessor.DragEvent;
import processing.touchProcessor.FlickEvent;
import processing.touchProcessor.PinchEvent;
import processing.touchProcessor.RotateEvent;
import processing.touchProcessor.TapEvent;
import processing.touchProcessor.TouchPoint;
import processing.touchProcessor.TouchProcessor;
import processing.touchProcessor.TouchProcessorListener;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaRecorder;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;
import apwidgets.*;

public class P5Sensors extends PApplet implements TouchProcessorListener
{
	TouchProcessor touch;
	Sensor accSensor = null;
	Sensor lightSensor = null;
	Sensor orientationSensor = null;
	MediaRecorder micRecorder = null; 
	
	MySensorEventListener accSensorListener = null;
	MySensorEventListener lightSensorListener = null;
	MySensorEventListener orientationSensorListener = null;
	
	static final private double EMA_FILTER = 0.6;
	private double mEMA = 0.0;
	
	float[] accData = new float[3];
	float[] accBuffer = new float[4]; // x, y, z, num of data
	float lightData = 0;
	float orientationData = 0;
	double rawMicData;
	float micData;
	float micLevelDamp = 0.4f;
	float micMaxLevel = 0.f;
	float micLearningSpeed = 1.f;//0.9999f;
	float touchX, touchY;
	float level;
	
	PFont font;
	
	OscP5 osc = null;
	NetAddress myNetAddress = null;
	String ipAddress = "192.168.0.171";
	
	Timer timer = null;
	
	PWidgetContainer widgetContainer; 
	PEditText ipAddressInput;
	PButton connectButton;
	
	boolean guiCreated = false;
	
	public void setup()
	{		
		touch = new TouchProcessor(this, this);
		size(screenWidth, screenHeight, A2D);
		frameRate(30);
		
		orientation(LANDSCAPE);
		
		font = createFont(PFont.list()[0], 12, true);
		textFont(font);
		
		
	}
	
	public void onResume()
	{
		super.onResume();				
				
		SensorManager sm = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
		if (accSensor == null) accSensor = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		if (lightSensor == null) lightSensor = sm.getDefaultSensor(Sensor.TYPE_LIGHT);
		if (orientationSensor == null) orientationSensor = sm.getDefaultSensor(Sensor.TYPE_ORIENTATION);
		if (accSensorListener == null) accSensorListener = new MySensorEventListener();
		if (lightSensorListener == null) lightSensorListener = new MySensorEventListener();
		if (orientationSensorListener == null) orientationSensorListener = new MySensorEventListener();
		
		sm.registerListener(accSensorListener, accSensor, SensorManager.SENSOR_DELAY_FASTEST);
		sm.registerListener(lightSensorListener, lightSensor, SensorManager.SENSOR_DELAY_FASTEST);
		sm.registerListener(orientationSensorListener, orientationSensor, SensorManager.SENSOR_DELAY_FASTEST);
		
		if (micRecorder == null) 
		{			
			micRecorder = new MediaRecorder();			
			micRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
			micRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
            micRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
			micRecorder.setOutputFile("/dev/null"); 
			mEMA = 0.0;
			
			try {
				micRecorder.prepare();				
			} catch (IllegalStateException e) {			
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}			
			micRecorder.start();			
		}
		
		if (timer == null) 
		{
			timer = new Timer();						
			timer.scheduleAtFixedRate(new TimerTask(){

				@Override
				public void run() 
				{
					if (micRecorder != null)
					{
						double amp = micRecorder.getMaxAmplitude();
		                mEMA = EMA_FILTER * amp + (1.0 - EMA_FILTER) * mEMA;
		                rawMicData = mEMA;
					}					
				}}, 3000, 30);					
		}
		
		
		if (osc == null)
		{
			osc = new OscP5(this, 7777);			
		}
		
		createGUI();				
	}		
		
	public void onPause()
	{				
		SensorManager sm = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
		
		if (accSensorListener != null) sm.unregisterListener(accSensorListener);
		if (lightSensorListener != null) sm.unregisterListener(lightSensorListener);
		if (orientationSensorListener != null) sm.unregisterListener(orientationSensorListener);
		
		if (timer != null)
		{
			timer.cancel();
			timer = null;
		}
		if (micRecorder != null)
		{
			micRecorder.stop();
			micRecorder.release();
			micRecorder = null;		
		}
		
		if (osc != null)
		{
			if (myNetAddress != null) osc.disconnect(myNetAddress);
			osc = null;
		}
		
		super.onPause();
	}
	
	public void createGUI()
	{
		if (guiCreated) return;
		
		widgetContainer = new PWidgetContainer(this);
		int inputBoxHeight = 40;
		int connectBoxWidth = 100;
		ipAddressInput = new PEditText(0, screenHeight - inputBoxHeight, screenWidth - connectBoxWidth, inputBoxHeight);
		ipAddressInput.setText(ipAddress);
		ipAddressInput.setTextColor(0, 100, 100, 255);
		
		connectButton = new PButton(screenWidth - connectBoxWidth, screenHeight - inputBoxHeight, connectBoxWidth, inputBoxHeight, "connect");
								
		connectButton.addOnClickWidgetListener(new OnClickWidgetListener()
		{
			@Override
			public void onClickWidget(PWidget widget) 
			{
				String ip = ipAddressInput.getText();
				if (ip != "")
				{
					ipAddress = ip;
					if (myNetAddress != null) myNetAddress = null;
					myNetAddress = new NetAddress(ipAddress, 12000);
					Toast.makeText(getApplicationContext(), "ip: " + ipAddress + " port: 12000", Toast.LENGTH_LONG).show();				   
				}				
			}		
		});
		widgetContainer.addWidget(ipAddressInput);
		widgetContainer.addWidget(connectButton);
		
		guiCreated = true;
	}
	
	void drawBar(String name, float value, int y)
	{
		int h = 20;
		noStroke();
		fill(20, 20, 20);
		rect(0, y, screenWidth, h);
		stroke(0, 255, 255);
		fill(0, 255, 255, 100);
		rect(0, y + 3, screenWidth * value, h - 7);		
		
		fill(50, 50, 50);
		text(name, 10, y + 15);
	}
	
	public void draw()
	{
		//background(255, 255, 255);
		background(0, 0, 0);
		
		if (micMaxLevel < rawMicData) micMaxLevel = (float)rawMicData;
		micData += (rawMicData - micData) * micLevelDamp;
		micMaxLevel *= micLearningSpeed;
		level = constrain(map(micData, 0, micMaxLevel, 0, 1), 0, 1);
		
		/////////////
				
		float maxVal = 10;
		float n = accBuffer[3];
		if (n > 0)
		{
			accData[0] = constrain(map(abs(accBuffer[0] / n), 0, maxVal, 0, 1), 0, 1);
	    	accData[1] = constrain(map(abs(accBuffer[1] / n), 0, maxVal, 0, 1), 0, 1);
	    	accData[2] = constrain(map(abs(accBuffer[2] / n), 0, maxVal, 0, 1), 0, 1);
	    	accBuffer[0] = accBuffer[1] = accBuffer[2] = accBuffer[3] = 0;
		}
    	////////////
		
		drawBar("acc X", accData[0], 30);
		drawBar("acc Y", accData[1], 60);
		drawBar("acc Z", accData[2], 90);
		drawBar("compass", orientationData, 130);
		drawBar("light", lightData, 170);
		drawBar("mic level", level, 210);
		
		touchX = 0;
		touchY = 0;
		
		ArrayList touchPoints = touch.getPoints(); 
		
		Iterator i = touchPoints.iterator();
		  while (i.hasNext ()) {
		    TouchPoint p = (TouchPoint)i.next();		    
		    touchX = p.x / (float)screenWidth;
		    touchY = p.y / (float)screenHeight;
		    break;
		  }
		
		  if (touchX != 0 && touchY != 0)
		  {
			  float diameter = 50;
			  stroke(0, 255, 0);
			  noFill();
			  ellipse(touchX * screenWidth, touchY * screenHeight, diameter, diameter);
			  fill(0, 255, 0);
			  ellipse(touchX * screenWidth, touchY * screenHeight, 8, 8);			  
		  }
		
		sendOsc();
	}
	
	public void sendOsc()
	{
		if (osc != null && myNetAddress != null)
		{			
			osc.send("/compass",
					new Object[]{
						new Float(orientationData)										
					}
					, myNetAddress
					);
			
			if (accData != null)
			osc.send("/accelerometer",
					new Object[]{
						new Float(accData[0]),
						new Float(accData[1]),
						new Float(accData[2])					
					}
					, myNetAddress
					);						
						
			osc.send("/light",
					new Object[]{
						new Float(lightData)										
					}
					, myNetAddress
					);
						
			osc.send("/amp",
					new Object[]{
						new Float(level)
					}
					, myNetAddress
					);
			if (touchX != 0 && touchY != 0)			
				osc.send("/touch",
					new Object[]{
						new Float(touchX),
						new Float(touchY)
					}
					, myNetAddress
					);
		}
	}
	
	

	@Override
	public void onFlick(FlickEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDrag(DragEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPinch(PinchEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onRotate(RotateEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTap(TapEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	public boolean surfaceTouchEvent(MotionEvent event) 
	{
		touch.touchEvent(event);
		return super.surfaceTouchEvent(event);
	}

	class MySensorEventListener implements SensorEventListener
	{	
		@Override
		public void onSensorChanged(SensorEvent event) 
		{
			int eventType = event.sensor.getType();
			
		    if(eventType == Sensor.TYPE_ACCELEROMETER) 
		    {
		    	accBuffer[0] += event.values[0];
		    	accBuffer[1] += event.values[1];
		    	accBuffer[2] += event.values[2];
		    	accBuffer[3] ++;		    			    	
		    }
		    else if (eventType == Sensor.TYPE_LIGHT)
		    {
		    	float maxValue = 1000;
		    	
		    	lightData = constrain(map(event.values[0], 0, maxValue, 0, 1), 0, 1);		    	
		    }		    
		    else if (eventType == Sensor.TYPE_ORIENTATION)
		    {
		    	orientationData = map(event.values[0], 0, 360, 0, 1);
		    }			
		}
		
		@Override
		public void onAccuracyChanged(Sensor arg0, int arg1) {
			// TODO Auto-generated method stub
			
		}

	}
}