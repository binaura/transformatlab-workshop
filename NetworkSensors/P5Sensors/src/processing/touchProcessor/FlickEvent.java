package processing.touchProcessor;

import processing.core.PVector;

///////////////////////////////////////////////////////////////////////////////////
public class FlickEvent extends TouchEvent { 

  float x;
  float y;
  PVector velocity;

  public FlickEvent(float x, float y, PVector velocity) {
    this.x = x; 
    this.y = y;
    this.velocity = velocity;
  }
}

