package processing.touchProcessor;

public interface TouchProcessorListener 
{
	public void onFlick(FlickEvent e);
	public void onDrag(DragEvent e);
	public void onPinch(PinchEvent e);
	public void onRotate(RotateEvent e);
	public void onTap(TapEvent e);
}
