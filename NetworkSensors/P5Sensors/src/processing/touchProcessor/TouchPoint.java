package processing.touchProcessor;
import processing.core.PApplet;

public class TouchPoint {
  
  public float x;
  public float y;
  public float px;
  public float py;
  public int id;
  
  // used for gesture detection
  public float angle;
  public float oldAngle;  
  public float pinch;
  public float oldPinch;
  
  PApplet app;

  //-------------------------------------------------------------------------------------
  public TouchPoint(PApplet _app, float x, float y, int id) 
  {
	  app = _app;
    this.x = x;
    this.y = y;
    this.px = x;
    this.py = y;
    this.id = id;  
  }

  //-------------------------------------------------------------------------------------
  void update(float x, float y) {
    px = this.x;
    py = this.y;
    this.x = x;
    this.y = y;
  }

  //-------------------------------------------------------------------------------------
  void initGestureData(float cx, float cy) {  
    pinch = oldPinch = app.dist(x, y, cx, cy);
    angle = oldAngle = app.atan2( (y-cy), (x-cx) );
  }
 
  //-------------------------------------------------------------------------------------
  // delta x -- int to get rid of some noise
  int dx() {
    return (int)(x - px);
  }
  
  //-------------------------------------------------------------------------------------
  // delta y -- int to get rid of some noise
  int dy() {
    return (int)(y - py);
  } 
  
  //-------------------------------------------------------------------------------------
  void setAngle(float angle) {
    oldAngle = this.angle;  
    this.angle = angle;
  }
  
  //-------------------------------------------------------------------------------------
  void setPinch(float pinch) {
     oldPinch = this.pinch;
     this.pinch = pinch; 
  }

}

