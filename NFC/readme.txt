Code for Transformatlab Workshop, 2014

NFC Reader & NFC Writer is built with processing using Android SDK. To install, download Processing and follow the instructions here: https://www.processing.org/download/

Note: You'll need an NFC capable Android device in order to run the code properly. You also need three NFC stickers, with ids "001", "002", "003". More info on NFC: http://en.wikipedia.org/wiki/Near_field_communication
