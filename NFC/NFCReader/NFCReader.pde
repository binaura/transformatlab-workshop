import android.content.Intent;
import android.os.Bundle;
import ketai.net.nfc.*;
KetaiNFC ketaiNFC;

PImage happy, confused, sad;
int showImage = 0;
float fillcolor;

void setup()
{   
  frameRate(60);
  orientation(PORTRAIT);
  //textAlign(CENTER, CENTER);
  textSize(106);
  imageMode(CENTER);
  happy = loadImage("happy.jpg");
  confused = loadImage("confused.jpg");
  sad = loadImage("sad.jpg");
}

void draw()
{
  background(255);
  
  if     (showImage == 0) {  fill(0,0,fillcolor,100); text("MOVE ME" + "\n" + "TO FIND" + "\n" + "TAGS", 10, 180); }
  else if(showImage == 1) image(happy,width/2,height/2);
  else if(showImage == 2) image(confused, width/2, height/2);
  else if(showImage == 3) image(sad,width/2, height/2);
  
  fillcolor = sin(frameCount / 30f) * 255;
}

void onNFCEvent(String txt)
{
  if     (txt.equals("001")) showImage = 1;
  else if(txt.equals("002")) showImage = 2;
  else if(txt.equals("003")) showImage = 3;
}

void mousePressed()  {  showImage = 0;  }

